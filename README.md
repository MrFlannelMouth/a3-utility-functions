### Flannel's A3 Utility Functions ###

While building some SQF prototypes, I'm starting to accumulate a modest library of utility functions.
I figured I might as well start packing them up and distributing the pack. Some of these functions are not difficult to write at all, but I might as well save you the trouble.

Right now, the pack contains mostly linear algebra functions (vectors, matrices, quaternions, that sort of stuff), but it will probably grow into different areas in the future.

On the [downloads](https://bitbucket.org/MrFlannelMouth/a3-utility-functions/downloads) page, there should be a link to an archive that contains a packed and signed pbo, as well as the source files in case you only need a single function for something.
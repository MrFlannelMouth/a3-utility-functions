class CfgPatches
{
	class Flan_util
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
	};
};

class CfgFunctions
{
	class Flan
	{
		// Vector and matrix utility functions
		class vectorMatrix
		{
			// Yes, some of these exist as BIS_fnc's, but this is a more complete set.
			file = "flan_util\functions\vectorMatrix";
			class vectorAdd {};
			class vectorDiff {};
			class vectorMultiply {};
			class vectorDotProduct {};
			class vectorNormalized {};
			class vectorMatrixProduct {};
			class matrixVectorProduct {};
			class matrixAdd {};
			class matrixDiff {};
			class matrixMultiply {};
			class matrixTransposed {};
			class matrixProduct {};
			class matrixDeterminant {};
			class matrixCofactors {};
			class matrixInverse {};
			class subMatrix {};
		};
		// Quaternion and rotation utility functions
		class quaternion
		{
			// The first 5 of these just call vector versions of the same operation,
			// they're included just in case you don't want to remember which operations
			// are different for quaternions
			file = "flan_util\functions\quaternion";
			class quaternionAdd {};
			class quaternionDiff {};
			class quaternionMultiply {};
			class quaternionDotProduct {};
			class quaternionNormalized {};
			class quaternionProduct {};
			class quaternionToFrom {};
			class quaternionSlerp {};
			class quaternionToMatrix {};
			class matrixToQuaternion {};
			class vectorDirAndUpFromQuaternion {};
			class rotationMatrix {};
		};
		// Map drawing and marker functions
		class map
		{
			file = "flan_util\functions\map";
			class findSplinePos {};
			class drawSmoothCurve {};
			class deleteCurve {};
		};
		// Misc utility functions
		class misc
		{
			file = "flan_util\functions\misc";
			class worldToModelDir {};
			class modelToWorldDir {};
			class rotateAroundAxis {};
			class circleSegment {};
		};

	};
};

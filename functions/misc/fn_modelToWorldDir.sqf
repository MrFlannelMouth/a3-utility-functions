/*
  Flan_fnc_modelToWorldDir

	Description:
	Converts model direction vector to world space (retains length)

	Parameter(s):
		0 (Required):	OBJECT - Object relative to which conversion should be
		1 (Required): ARRAY - Model space direction vector (3D)

	Returns: ARRAY

  Example usage: worldDir = [object, modelDir] call Flan_fnc_modelToWorldDir
*/

params ["_obj", "_dir"];
private ["_output"];

// Conversion to ASL for accuracy in case of wave difference between positions
_output = (AGLToASL (_obj modelToWorldVisual _dir)) vectorDiff (AGLToASL (_obj modelToWorldVisual [0,0,0]));

_output

/*
  Flan_fnc_worldToModelDir

	Description:
	Converts world direction vector to model space (retains length)

	Parameter(s):
		0 (Required):	OBJECT - Object relative to which conversion should be
		1 (Required): ARRAY - World space direction vector (3D)

	Returns: ARRAY

  Example usage: modelDir = [object, worldDir] call Flan_fnc_worldToModelDir
*/

params ["_obj", "_dir"];
private ["_output"];

// Conversion back and forth to/from ASL and AGL for accuracy
// -> Ignores waves from output of modelToWorld
// -> Adds waves back in for proper AGL input of worldToModel
_output = _obj worldToModelVisual (ASLToAGL ((AGLToASL (_obj modelToWorldVisual [0,0,0])) vectorAdd _dir));

_output

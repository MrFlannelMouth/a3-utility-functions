/*
  Flan_fnc_rotateAroundAxis

	Description:
  Rotate object some angle around arbitrary modelspace axis
  Axis anchor point default is [0,0,0], but this point is not always in the center of the bounding box
  Uses quaternions, but has to calculate current quaternion every time (might be too expensive)

	Parameter(s):
		0 (Required):	OBJECT - Object to rotate
		1 (Required): NUMBER - Angle to rotate object
    2 (Required): ARRAY - Model space axis to rotate object around (3D)
    3 (Optional): ARRAY - Model space position to anchor rotation axis in (point stays in place) (default: [0,0,0])

	Returns: BOOL - true

  Example usage: [object, angle, axis, (axisAnchor)] call Flan_fnc_rotateAroundAxis
*/

// TODO: Write a function that works for objects where quaternion remains known, not affected by outside actors
//       Could be a lot cheaper, depending on how expensive fnc_rotationMatrix and fnc_matrixToQuaternion are

params ["_obj", "_angle", "_axis", ["_anchor", [0, 0, 0]]];
private ["_rotQuat", "_baseQuat", "_quat", "_dirUp", "_oldAxisWorldPos", "_newAxisWorldPos", "_delta"];

// Check if anchor correction is needed
if (!(_anchor isEqualTo [0, 0, 0])) then
{
  _oldAxisWorldPos = AGLToASL (_obj modelToWorldVisual _anchor);
};

_rotQuat = [cos (_angle / 2)] + (_axis vectorMultiply (sin (_angle / 2)));
_baseQuat = [[_obj] call Flan_fnc_rotationMatrix] call Flan_fnc_matrixToQuaternion;

_quat = [_baseQuat, _rotQuat] call Flan_fnc_quaternionProduct;
_quat = [_quat] call Flan_fnc_quaternionNormalized;

_dirUp = [_quat] call Flan_fnc_vectorDirAndUpFromQuaternion;

_obj setVectorDirAndUp _dirUp;

// Do anchor correction if needed
if (!(_anchor isEqualTo [0, 0, 0])) then
{
  _newAxisWorldPos = AGLToASL (_obj modelToWorldVisual _anchor);
  _delta = _oldAxisWorldPos vectorDiff _newAxisWorldPos;
  _obj setPosWorld ((getPosWorld _obj) vectorAdd _delta);
};

true

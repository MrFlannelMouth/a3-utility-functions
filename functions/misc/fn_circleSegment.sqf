/*
  Flan_fnc_circleSegment

	Description:
	Calculates surface area of circle segment above line at height h below the top of a circle
  Example: http://mathworld.wolfram.com/images/eps-gif/CircularSegment_1001.gif

	Parameter(s):
		0 (Required):	NUMBER - Circle radius
		1 (Required): NUMBER - Height below circle top

	Returns: NUMBER

  Example usage: area = [radius, height] call Flan_fnc_circleSegment
*/

params ["_R", "_h"];
private ["_out"];

_out = (_R^2) * acos ((_R - _h) / _R) - (_R - _h) * sqrt (2 * _R * _h - (_h^2));

_out

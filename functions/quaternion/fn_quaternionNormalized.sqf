/*
  Flan_fnc_quaternionNormalized

	Description:
	Find quaternion in the same direction, but with unit length

	Parameter(s):
		0 (Required):	ARRAY - Vector

	Returns: ARRAY

  Example usage: result = [q] call Flan_fnc_quaternionNormalized
*/

params ["_q"];
private ["_out"];

_out = [_q] call Flan_fnc_vectorNormalized;

_out

#include "..\macros.hpp"
/*
  Flan_fnc_vectorDirAndUpFromQuaternion

	Description:
	// Returns vectorDir and vectorUp from quaternion rotation

	Parameter(s):
		0 (Required):	ARRAY - Quaternion

	Returns: ARRAY of ARRAYs ([vectorDir, vectorUp], i.e. suitable as input for setVectorDirAndUp)

  Example usage: result = [q] call Flan_fnc_vectorDirAndUpFromQuaternion
*/

params ["_q"];
private ["_R", "_dir", "_up"];

_R = [_q] call Flan_fnc_quaternionToMatrix;

_dir = _R select 1;
_up = _R select 2;

[_dir, _up]

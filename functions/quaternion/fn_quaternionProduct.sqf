#include "..\macros.hpp"
/*
  Flan_fnc_quaternionProduct

	Description:
	Product for quaternions, represents rotation q1 followed by rotation q2

	Parameter(s):
		0 (Required):	ARRAY - First quaternion
		1 (Required): ARRAY - Second quaternion

	Returns: ARRAY

  Example usage: result = [q1, q2] call Flan_fnc_quaternionProduct
*/

params ["_q1", "_q2"];
private ["_out"];

_out = [1, 0, 0, 0];

SET_S(_out, S(_q1) * S(_q2) - (V(_q1) vectorDotProduct V(_q2)));
SET_V(_out, (           (V(_q2) vectorMultiply S(_q1))
              vectorAdd (V(_q1) vectorMultiply S(_q2)))
              vectorAdd (V(_q1) vectorCrossProduct V(_q2))
            );

_out = [_out] call Flan_fnc_quaternionNormalized;

_out

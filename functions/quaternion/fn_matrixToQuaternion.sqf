#include "..\macros.hpp"
/*
  Flan_fnc_matrixToQuaternion

	Description:
	Returns the quaternion corresponding to a rotation matrix

	Parameter(s):
		0 (Required):	ARRAY of ARRAYs - 3x3 rotation matrix to translate

	Returns: ARRAY of ARRAYs

  Example usage: result = [R] call Flan_fnc_matrixToQuaternion
*/

params ["_R"];
private ["_q", "_diagSum", "_num1", "_num2"];

_q = [1, 0, 0, 0];
_diagSum = ITEM(_R, 0, 0) + ITEM(_R, 1, 1) + ITEM(_R, 2, 2);

switch (true) do
{
  case (_diagSum >= 0.0):
    {
      _num1 = sqrt (_diagSum + 1);
      _num2 = 0.5 / _num1;
      SET_S(_q, _num1 * 0.5);
      // SET_V was misbehaving, switching to simple triple set
      _q set [1, _num2 * (ITEM(_R, 1, 2) - ITEM(_R, 2, 1))];
      _q set [2, _num2 * (ITEM(_R, 2, 0) - ITEM(_R, 0, 2))];
      _q set [3, _num2 * (ITEM(_R, 0, 1) - ITEM(_R, 1, 0))];
    };
  case ((ITEM(_R, 0, 0) >= ITEM(_R, 1, 1)) && (ITEM(_R, 0, 0) >= ITEM(_R, 2, 2))):
    {
      _num1 = sqrt (1 + ITEM(_R, 0, 0) - ITEM(_R, 1, 1) - ITEM(_R, 2, 2));
      _num2 = 0.5 / _num1;
      SET_S(_q, _num2 * (ITEM(_R, 1, 2) - ITEM(_R, 2, 1)));

      _q set [1, _num1 * 0.5];
      _q set [2, _num2 * (ITEM(_R, 0, 1) + ITEM(_R, 1, 0))];
      _q set [3, _num2 * (ITEM(_R, 0, 2) + ITEM(_R, 2, 0))];
    };
  case (ITEM(_R, 1, 1) >= ITEM(_R, 2, 2)):
    {
      _num1 = sqrt (1 + ITEM(_R, 1, 1) - ITEM(_R, 0, 0) - ITEM(_R, 2, 2));
      _num2 = 0.5 / _num1;
      SET_S(_q, _num2 * (ITEM(_R, 2, 0) - ITEM(_R, 0, 2)));

      _q set [1, _num2 * (ITEM(_R, 1, 0) + ITEM(_R, 0, 1))];
      _q set [2, _num1 * 0.5];
      _q set [3, _num2 * (ITEM(_R, 2, 1) + ITEM(_R, 1, 2))];
    };
  default
    {
      _num1 = sqrt (1 + ITEM(_R, 2, 2) - ITEM(_R, 0, 0) - ITEM(_R, 1, 1));
      _num2 = 0.5 / _num1;
      SET_S(_q, _num2 * (ITEM(_R, 0, 1) - ITEM(_R, 1, 0)));

      _q set [1, _num2 * (ITEM(_R, 2, 0) + ITEM(_R, 0, 2))];
      _q set [2, _num2 * (ITEM(_R, 2, 1) + ITEM(_R, 1, 2))];
      _q set [3,  _num1 * 0.5];
    };
};

_q = [_q] call Flan_fnc_quaternionNormalized;

_q

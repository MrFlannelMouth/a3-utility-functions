#include "..\macros.hpp"
/*
  Flan_fnc_quaternionToFrom

	Description:
	Find quaternion corresponding to the shortest arc rotation from unit vector v1 to v2

	Parameter(s):
		0 (Required):	ARRAY - Starting vector (3D)
		1 (Required): ARRAY - Target vector (3D)

	Returns: ARRAY

  Example usage: result = [v1, v2] call Flan_fnc_quaternionToFrom
*/

params ["_v1", "_v2"];
private ["_quat"];

// TODO: _v1 == _v2 or _v1 == -_v2 special cases

_v = _v1 vectorAdd _v2;
_v = vectorNormalized _v;

_angle = _v vectorDotProduct _v2;
_axis = _v vectorCrossProduct _v2;

_quat = [_angle] + _axis;
_quat = [_quat] call Flan_fnc_quaternionNormalized;

_quat

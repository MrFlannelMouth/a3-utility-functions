/*
  Flan_fnc_quaternionAdd

	Description:
	Addition for quaternions

	Parameter(s):
		0 (Required):	ARRAY - First quaternion
		1 (Required): ARRAY - Second quaternion

	Returns: ARRAY

  Example usage: result = [q1, q2] call Flan_fnc_quaternionAdd
*/

params ["_q1", "_q2"];
private ["_out"];

_out = [_q1, _q2] call Flan_fnc_vectorAdd;

_out

#include "..\macros.hpp"
/*
  Flan_fnc_quaternionToMatrix

	Description:
	Returns 3x3 rotation matrix based on quaternion rotation (matrix is defined as three column vectors)

	Parameter(s):
		0 (Required):	ARRAY - Quaternion

	Returns: ARRAY of ARRAYs

  Example usage: result = [q] call Flan_fnc_quaternionToMatrix
*/

params ["_q"];
private ["_s", "_v", "_R"];

_s = S(_q);
_v = V(_q);

_R = [
  [
    1 - 2 * (_v select 1)^2 - 2 * (_v select 2)^2,
    2 * (_v select 0) * (_v select 1) + 2 * _s * (_v select 2),
    2 * (_v select 0) * (_v select 2) - 2 * _s * (_v select 1)
  ],
  [
    2 * (_v select 0) * (_v select 1) - 2 * _s * (_v select 2),
    1 - 2 * (_v select 0)^2 - 2 * (_v select 2)^2,
    2 * (_v select 1) * (_v select 2) + 2 * _s * (_v select 0)
  ],
  [
    2 * (_v select 0) * (_v select 2) + 2 * _s * (_v select 1),
    2 * (_v select 1) * (_v select 2) - 2 * _s * (_v select 0),
    1 - 2 * (_v select 0)^2 - 2 * (_v select 1)^2
  ]
];

_R

/*
  Flan_fnc_rotationMatrix

	Description:
	Get rotation matrix of an object (3x3, column vectors [vectorRight, vectorDir, vectorUp])

	Parameter(s):
		0 (Required):	OBJECT - Object to find rotation matrix for

	Returns: ARRAY of ARRAYs

  Example usage: result = [object] call Flan_fnc_rotationMatrix
*/

params ["_obj"];
private ["_R"];
// Subarrays are columns in the matrix. Also, BI, why no vectorRight? :c
_R = [(vectorDirVisual _obj) vectorCrossProduct (vectorUpVisual _obj),
      vectorDirVisual _obj,
      vectorUpVisual _obj];

_R

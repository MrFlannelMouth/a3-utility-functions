/*
  Flan_fnc_quaternionDotProduct

	Description:
	Dot product for quaternions
  (spoiler alert, it's just the dot product for normal vectors,
    but this is included for anyone who forgot that)

	Parameter(s):
		0 (Required):	ARRAY - First quaternion
		1 (Required): ARRAY - Second quaternion

	Returns: NUMBER

  Example usage: result = [q1, q2] call Flan_fnc_quaternionDotProduct
*/

params ["_q1", "_q2"];
private ["_out"];

_out = [_q1, _q2] call Flan_fnc_vectorDotProduct;

_out

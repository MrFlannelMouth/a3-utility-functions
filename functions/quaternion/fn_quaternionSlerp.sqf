#include "..\macros.hpp"
/*
  Flan_fnc_quaternionSlerp

	Description:
  Interpolate between two (unit) quaternions, based on a factor [0,1]

	Parameter(s):
    0 (Required):	ARRAY - First quaternion
    1 (Required): ARRAY - Second quaternion
    2 (Required): NUMBER - Factor in interval [0,1], describing contribution of second quaternion

	Returns: ARRAY

  Example usage: result = [quat1, quat2, factor] call Flan_fnc_quaternionSlerp
*/

params ["_q1", "_q2", "_t"];
private ["_dot", "_theta", "_q3", "_out"];
_out = [1, 0, 0, 0];

// Compute the cosine of the angle between the two vectors.
_dot = [_q1, _q2] call Flan_fnc_quaternionDotProduct;

// If the inputs are very similar, linearly interpolate
_dotThreshold = 0.9995;
if (_dot > _dotThreshold) then {
  // out = q1 + t * (q2 - q1)
  _out = [_q1,[[_q2, _q1] call Flan_fnc_quaternionDiff, _t] call Flan_fnc_quaternionMultiply
          ] call Flan_fnc_quaternionAdd;
  _out = [_out] call Flan_fnc_quaternionNormalized;
} else {
  _dot = (_dot max -1) min 1;
  _theta = acos _dot;
  _theta = _theta * _t;

  _q3 = [_q2, [_q1, _dot] call Flan_fnc_quaternionMultiply] call Flan_fnc_quaternionDiff;
  _q3 = [_q3] call Flan_fnc_quaternionNormalized;

  _out = [[_q1, cos _theta] call Flan_fnc_quaternionMultiply,
          [_q3, sin _theta] call Flan_fnc_quaternionMultiply] call Flan_fnc_quaternionAdd;
};

_out

/*
  Flan_fnc_quaternionMultiply

	Description:
	Quaternion-scalar multiplication
  (spoiler alert, it's just the multiplication for normal vectors,
    but this is included for anyone who forgot that)

	Parameter(s):
		0 (Required):	ARRAY - Quaternion
		1 (Required): NUMBER - Scalar

	Returns: ARRAY

  Example usage: result = [q, s] call Flan_fnc_quaternionMultiply
*/

params ["_q", "_s"];
private ["_out"];

_out = [_q, _s] call Flan_fnc_vectorMultiply;

_out

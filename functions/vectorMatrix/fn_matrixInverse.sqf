#include "..\macros.hpp"
/*
  Flan_fnc_matrixInverse

	Description:
	Find the inverse matrix for a given square matrix (i.e. multiplication would give identity matrix)

	Parameter(s):
		0 (Required):	ARRAY of ARRAYs - Matrix (nested arrays are column vectors)

	Returns: ARRAY of ARRAYs

  Example usage: result = [m] call Flan_fnc_matrixInverse
*/

params ["_m"];
private ["_out", "_det"];

_det = [_m] call Flan_fnc_matrixDeterminant;
if (_det == 0) exitwith {diag_log "Zero determinant, no inverse"; [[0]]};

// Inverse = 1/det * cofactors^T
_out = [[[_m] call Flan_fnc_matrixCofactors] call Flan_fnc_matrixTransposed, 1 / _det] call Flan_fnc_matrixMultiply;

_out

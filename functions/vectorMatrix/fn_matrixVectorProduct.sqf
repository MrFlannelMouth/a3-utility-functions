/*
  Flan_fnc_matrixVectorProduct

	Description:
  Multiply matrix with column vector. For arbitrary (but equal where needed) sizes.

	Parameter(s):
		0 (Required):	ARRAY of ARRAYs - Matrix
		1 (Required): ARRAY - Vector

	Returns: ARRAY

  Example usage: result = [m, v] call Flan_fnc_matrixVectorProduct
*/

params ["_m", "_v"];
private ["_out", "_sizeY"];
_out = [];
_sizeY = count (_m select 0); // Matrix vertical length

// Need to do some awkward dot product (compared to vector-matrix product)
// because of the column structure of matrices
for [{private _j= 0}, {_j < _sizeY}, {_j = _j + 1}] do {
  private ["_sum"];
  _sum = 0;
  {
    _sum = _sum + _x * ((_m select _forEachIndex) select _j);
  } forEach _v;
  _out set [_j, _sum];
};

_out

/*
  Flan_fnc_vectorMatrixProduct

	Description:
  Multiply row vector with a matrix. For arbitrary (but equal where needed) sizes.

	Parameter(s):
		0 (Required):	ARRAY - Vector
		1 (Required): ARRAY of ARRAYs - Matrix

	Returns: ARRAY

  Example usage: result = [v, m] call Flan_fnc_vectorMatrixProduct
*/

params ["_v", "_m"];
private ["_out"];
_out = [];

{
  _out set [_forEachIndex, [_v, _x] call Flan_fnc_vectorDotProduct];
} forEach _m;

_out

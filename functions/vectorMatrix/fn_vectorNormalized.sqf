/*
  Flan_fnc_vectorNormalized

	Description:
	Find vector in the same direction, but with unit length

	Parameter(s):
		0 (Required):	ARRAY - Vector

	Returns: ARRAY

  Example usage: result = [v] call Flan_fnc_vectorNormalized
*/

params ["_v"];
private ["_size", "_out"];
_out = [1, 0, 0, 0];

// Find current size
_size = 0;
{
  _size = _size + _x ^ 2;
} forEach _v;

// Check size just in case
if (_size == 0 || !finite _size) exitWith {[1,0,0,0]};

// Scale each element
{
  _out set [_forEachIndex, _x / _size];
} forEach _v;

_out

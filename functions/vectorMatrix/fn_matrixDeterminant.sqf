#include "..\macros.hpp"
/*
  Flan_fnc_matrixDeterminant

	Description:
	Find the determinant of a given square matrix

	Parameter(s):
		0 (Required):	ARRAY of ARRAYs - Matrix (nested arrays are column vectors)

	Returns: NUMBER

  Example usage: result = [m] call Flan_fnc_matrixDeterminant
*/

params ["_m"];
private ["_out", "_size", "_sign"];
_size = count _m;

// If possible, do the easy ones
if (_size == 1) exitWith {(_m select 0) select 0};
if (_size == 2) exitWith {ITEM(_m, 0, 0) * ITEM(_m, 1, 1) -
                          ITEM(_m, 0, 1) * ITEM(_m, 1, 0)};

// Otherwise, divide and conquer
_out = 0;
for [{private _i = 0}, {_i < _size}, {_i = _i + 1}] do {
  // Expansion by cofactors, multiply this value (with proper sign +1/-1)
  // by determinant for submatrix without this row and column
  _sign = (-1) ^ _i;
  maxi = _i;
  _out = _out + _sign * ITEM(_m, _i, 0) * ([[_m, _i, 0] call Flan_fnc_subMatrix] call Flan_fnc_matrixDeterminant);
};

_out

#include "..\macros.hpp"
/*
  Flan_fnc_matrixProduct

	Description:
	Multiply two matrices (where there are as many columns in the first matrix as rows in the second)
  With m1 having n x m elements (n rows, m columns), m2 has m x p, output has n x p
  (Apologies for doing the size stuff the wrong way around by making index M_ij the i'th column and j'th row )

	Parameter(s):
		0 (Required):	ARRAY of ARRAYs - First matrix
    1 (Required):	ARRAY of ARRAYs - Second matrix

	Returns: ARRAY of ARRAYs

  Example usage: result = [m1, m2] call Flan_fnc_matrixProduct
*/

params ["_mat1", "_mat2"];
private ["_out", "_n", "_m1", "_m2", "_p", "_v"];
_m1 = count _mat1; // Yep, wrong way around for the 'n x m * m x p = n x p' notation
_n = count (_mat1 select 0);
_p = count _mat2;
_m2 = count (_mat2 select 0);

if (_m1 != _m2) exitWith {diag_log "You done goofed [Flan_fnc_matrixProduct]"; []};

// Fill output with the appropriate number of empty columns to not break 'select'
_out = [];
for [{private _a= 0}, {_a < _p}, {_a = _a + 1}] do {
  _out set [_a, []];
};

// Now do actual multiplication
for [{private _i= 0}, {_i < _p}, {_i = _i + 1}] do {
  for [{private _j= 0}, {_j < _n}, {_j = _j + 1}] do {
    // Value at i,j is essentially a dot product
    // But the first vector is a row of the first matrix:
    _v = [];
    for [{private _k= 0}, {_k < _m1}, {_k = _k + 1}] do {
      _v set [_k, ITEM(_mat1, _k, _j)]; // TODO: Write a function for this
    };

    // We have the two vectors, do the dot product
    (_out select _i) set [_j, [_v, _mat2 select _i] call Flan_fnc_vectorDotProduct];
  };
};

_out

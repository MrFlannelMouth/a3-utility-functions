#include "..\macros.hpp"
/*
  Flan_fnc_subMatrix

	Description:
	Find the square submatrix of a given square matrix obtained by excluding a row and column

	Parameter(s):
		0 (Required):	ARRAY of ARRAYs - Matrix (nested arrays are column vectors)
    1 (Required):	NUMBER - Column index to exclude
    2 (Required):	NUMBER - Row index to exclude

	Returns: ARRAY of ARRAYs

  Example usage: result = [m, column, row] call Flan_fnc_subMatrix
*/

params ["_m", "_column", "_row"];
private ["_out", "_size", "_modifierI", "_modifierJ"];
_size = count _m;

// Fill output with the appropriate number of empty columns to not break 'select'
_out = [];
for [{private _a= 0}, {_a < (_size - 1)}, {_a = _a + 1}] do {
  _out set [_a, []];
};

for [{private _i= 0}, {_i < _size}, {_i = _i + 1}] do {
  for [{private _j= 0}, {_j < _size}, {_j = _j + 1}] do {
    if (_i != _column && _j != _row) then { // Skip excluded rows
      // Set target element to lower rows/columns if we're past the exluded ones
      _modifierI = 0;
      _modifierJ = 0;
      if (_i > _column) then { _modifierI = -1;};
      if (_j > _row) then { _modifierJ = -1;};
      (_out select (_i + _modifierI)) set [_j + _modifierJ, ITEM(_m, _i, _j)];
    };
  };
};

_out

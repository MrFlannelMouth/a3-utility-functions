/*
  Flan_fnc_vectorMultiply

	Description:
	Vector-scalar multiplication for vector of arbitrary length

	Parameter(s):
		0 (Required):	ARRAY - Vector
		1 (Required): NUMBER - Scalar

	Returns: ARRAY

  Example usage: result = [v, s] call Flan_fnc_vectorMultiply
*/

params ["_v", "_s"];
private ["_out"];
_out = [];

{
  _out set [_forEachIndex, _x * _s];
} forEach _v;

_out

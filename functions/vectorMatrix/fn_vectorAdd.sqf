/*
  Flan_fnc_vectorAdd

	Description:
	Addition for vectors of arbitrary (equal) length

	Parameter(s):
		0 (Required):	ARRAY - First vector
		1 (Required): ARRAY - Second vector

	Returns: ARRAY

  Example usage: result = [v1, v2] call Flan_fnc_vectorAdd
*/

params ["_v1", "_v2"];
private ["_out"];
_out = +_v1;

{
  _out set [_forEachIndex, _x + (_v2 select _forEachIndex)];
} forEach _out;

_out

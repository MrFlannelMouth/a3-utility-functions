#include "..\macros.hpp"
/*
  Flan_fnc_matrixCofactors

	Description:
	Find the matrix of cofactors for a given square matrix

	Parameter(s):
		0 (Required):	ARRAY of ARRAYs - Matrix (nested arrays are column vectors)

	Returns: ARRAY of ARRAYs

  Example usage: result = [m] call Flan_fnc_matrixCofactors
*/

params ["_m"];
private ["_out", "_size", "_sign"];
_out = +_m; // Copy _m to easily get proper size
_size = count _out;

for [{private _i = 0}, {_i < _size}, {_i = _i + 1}] do {
  for [{private _j = 0}, {_j < _size}, {_j = _j + 1}] do {
    _sign = ((-1)^_i) * ((-1)^_j);
    (_out select _i) set [_j, _sign * ([[_m, _i, _j] call Flan_fnc_subMatrix] call Flan_fnc_matrixDeterminant)];
  };
};

_out

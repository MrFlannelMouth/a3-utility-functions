#include "..\macros.hpp"
/*
  Flan_fnc_matrixTransposed

	Description:
	Find the transposed version of a given square matrix (i.e. flip it across the diagonal)

	Parameter(s):
		0 (Required):	ARRAY of ARRAYs - Matrix (nested arrays are column vectors)

	Returns: ARRAY of ARRAYs

  Example usage: result = [m] call Flan_fnc_matrixTransposed
*/

params ["_m"];
private ["_out", "_sizeX", "_sizeY"];
_out = +_m;
_sizeX = count _out;
_sizeY = count (_out select 0); // Should be square, but w/e

for [{private _i= 0}, {_i < _sizeX}, {_i = _i + 1}] do {
  for [{private _j= 0}, {_j < _sizeY}, {_j = _j + 1}] do {
    (_out select _i) set [_j, ITEM(_m, _j, _i)];
  };
};

_out

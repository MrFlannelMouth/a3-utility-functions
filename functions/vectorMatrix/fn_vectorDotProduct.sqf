/*
  Flan_fnc_vectorDotProduct

	Description:
	Vector dot product for vectors of arbitrary length

	Parameter(s):
		0 (Required):	ARRAY - Vector 1
		1 (Required): ARRAY - Vector 2

	Returns: NUMBER

  Example usage: result = [v1, v2] call Flan_fnc_vectorDotProduct
*/

params ["_v1", "_v2"];
private ["_out"];
_out = 0;
{
  _out = _out + _x * (_v2 select _forEachIndex);
} forEach _v1;

_out

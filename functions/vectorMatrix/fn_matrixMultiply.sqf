#include "..\macros.hpp"
/*
  Flan_fnc_matrixMultiply

	Description:
	Matrix-scalar multiplication for matrix of arbitrary size

	Parameter(s):
		0 (Required):	ARRAY of ARRAYs - Matrix (nested arrays are column vectors)
		1 (Required): NUMBER - Scalar

	Returns: ARRAY of ARRAYs

  Example usage: result = [m, s] call Flan_fnc_matrixMultiply
*/

params [["_matrix", [[]]], "_scalar"];
private ["_out", "_sizeX", "_sizeY"];
_out = +_matrix;
_sizeX = count _out;
_sizeY = count (_out select 0);

for [{private _i= 0}, {_i < _sizeX}, {_i = _i + 1}] do {
  for [{private _j= 0}, {_j < _sizeY}, {_j = _j + 1}] do {
    (_out select _i) set [_j, _scalar * ITEM(_out, _i, _j)];
  };
};

_out

#include "..\macros.hpp"
/*
  Flan_fnc_matrixAdd

	Description:
	Matrix addition for matrix of arbitrary size

	Parameter(s):
		0 (Required):	ARRAY of ARRAYs - Matrix (nested arrays are column vectors)
		1 (Required):	ARRAY of ARRAYs - Matrix

	Returns: ARRAY of ARRAYs

  Example usage: result = [m1, m2] call Flan_fnc_matrixAdd
*/

params ["_m1", "_m2"];
private ["_out", "_sizeX", "_sizeY"];
_out = +_m1;
_sizeX = count _out;
_sizeY = count (_out select 0);

for [{private _i= 0}, {_i < _sizeX}, {_i = _i + 1}] do {
  for [{private _j= 0}, {_j < _sizeY}, {_j = _j + 1}] do {
    (_out select _i) set [_j, ITEM(_m1, _i, _j) + ITEM(_m2, _i, _j)];
  };
};

_out

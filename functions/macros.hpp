// Quaternion macros, Q = [s, vx, vy, vz]
#define S(QUAT) (QUAT select 0)
#define V(QUAT) ([QUAT select 1, QUAT select 2, QUAT select 3])
#define SET_S(QUAT, S) (QUAT set [0, S])
#define SET_V(QUAT, V) QUAT set [1, V select 0]; QUAT set [2, V select 1]; QUAT set [3, V select 2]

// Matrix macros (matrix nested arrays are the column vectors)
#define ITEM(MATRIX,I,J) ((MATRIX select I) select J)
#define SET_ITEM(MATRIX,I,J,VALUE) (MATRIX select I) set [J, VALUE]

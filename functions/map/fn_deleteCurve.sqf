/*
  Flan_fnc_deleteCurve

	Description:
  Delete curve made by Flan_fnc_drawSmoothCurve

	Parameter(s):
		0 (Required):	NUMBER - ID of drawing event handler (returned by Flan_fnc_drawSmoothCurve)

	Returns: BOOL

  Example usage: result = [id] call Flan_fnc_deleteCurve
*/

params ["_id"];
(findDisplay 12 displayCtrl 51) ctrlRemoveEventHandler ["Draw", _id];

true

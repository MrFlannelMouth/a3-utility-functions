/*
  Flan_fnc_drawSmoothCurve

	Description:
  Draw smooth curve through 2D map positions (e.g. markers). Draws a cardinal spline.
  Returns ID of registered set of lines.

	Parameter(s):
		0 (Required):	ARRAY - Array of points to draw curve through (2D)
    1 (Required):	ARRAY - Color of line ([R,G,B,A] format)
    2 (Required):	NUMBER - Resolution, number of segments to draw between any two points
    3 (Optional):	NUMBER - Tension of curve. Value between 0 and 1, where 1 results
                            in straight lines between points (default: 0.5)

	Returns: NUMBER

  Example usage: id = [positionArray, color, resolution, curveTension] call Flan_fnc_drawSmoothCurve
*/

params ["_posArray", "_color", "_res", ["_tension", 0.5]];
private ["_out", "_lineCount", "_drawArray", "_u", "_lastPos"];
_drawArray = [];
_lineCount = ((count _posArray) - 1) * _res; // Resolution == nr. of segments between two points

// Find line segments per posArray interval
{
  if (_forEachIndex < (count (_posArray) - 1)) then {
    _lastPos = _x; // First position of this point-point segment
    for [{private _i = 1}, {_i <= _res}, {_i = _i + 1}] do {
      _u = _i / _res;
      _splinePos = [_posArray, _u, _forEachIndex, _tension] call Flan_fnc_findSplinePos;

      // Add lastpos->splinepos line
      _drawArray = _drawArray + [[_lastPos, _splinePos]];

      // Save lastpos
      _lastPos = _splinePos;
    };
  };
} forEach _posArray;

// Add drawing event handler and remember handle
_out = (findDisplay 12 displayCtrl 51) ctrlAddEventHandler ["Draw", format [
  " private ['_toDraw', '_c'];
    _toDraw = %1;
    _c = %2;
    {
      (_this select 0) drawLine [
        _x select 0,
        _x select 1,
        _c
      ];
    } forEach _toDraw;",_drawArray, _color]];

_out

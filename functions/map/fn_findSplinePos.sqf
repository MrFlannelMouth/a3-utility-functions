/*
  Flan_fnc_findSplinePos

	Description:
  Find position on cardinal spline after travelling a certain portion u of the path between P_i and P_i+1

	Parameter(s):
		0 (Required):	ARRAY - List of anchor point positions, 2D or 3D (or nD, see if I care)
		1 (Required): NUMBER - Portion along the path between point i and i+1 to find position
    2 (Required): NUMBER - Index i of last visited point (i.e. find position between i and i+1)
    3 (Required): NUMBER - Spline tension in interval [0,1] where 1 is maximal tension (sharp turns)

	Returns: ARRAY

  Example usage: posVector = [points, u, index, splineTension] call Flan_fnc_findSplinePos
*/

params ["_points", "_u", "_index", "_tau"];
private ["_uVect", "_tauMatrix", "_posVect", "_factorVec", "_outPos", "_p1", "_p2", "_p3", "_p4"];
_outPos = [0, 0];
_outPos set [(count (_points select 0)) - 1, 0]; // Add third zero if needed (proper length)
if (_tau == 0) then { // Divide by zero etc etc
  _tau = 0.1;
};

_uVect = [_u^3, _u^2, _u, 1];
_tauMatrix = [[-1, 2, -1, 0],
              [-1 + 2/_tau, 1 - 3/_tau, 0, 1/_tau],
              [1 - 2/_tau, -2 + 3/_tau, 1, 0],
              [1, -1, 0, 0]];
_tauMatrix = [_tauMatrix, _tau] call Flan_fnc_matrixMultiply;
_p1 = if (_index == 0) then {_points select _index} else {_points select _index - 1};
_p2 = _points select _index;
_p3 = if (_index >= ((count _points) - 1)) then {_points select _index} else {_points select _index + 1};
_p4 = if (_index >= ((count _points) - 2)) then {_points select _index} else {_points select _index + 2};
_posVect = [_p1, _p2, _p3, _p4];

_factorVec = [_uVect, _tauMatrix] call Flan_fnc_vectorMatrixProduct;

{
  _outPos = [_outPos, [_x, _factorVec select _forEachIndex] call Flan_fnc_vectorMultiply] call Flan_fnc_vectorAdd;
} forEach _posVect;

_outPos
